package main

import (
	"log"

	"hartree.stfc.ac.uk/hbaas-server/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		log.Fatal("Unable to run root command:", err)
	}
}
